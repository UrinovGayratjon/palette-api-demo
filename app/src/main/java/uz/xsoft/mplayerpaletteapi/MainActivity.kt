package uz.xsoft.mplayerpaletteapi

import android.app.Activity
import android.content.*
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.view.Window
import android.view.WindowManager
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {
    var isBinded = false
    lateinit var mediaPlaybackService: MediaPlaybackService
    var elapsedTime = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageButtonPlayPause.setOnClickListener {
            val resId: Int
            if (mediaPlaybackService.isPlaying()) {
                resId = R.drawable.ic_play_circle_outline_black_48dp
                mediaPlaybackService.pause()
            } else {
                resId = R.drawable.ic_pause_circle_outline_black_48dp
                mediaPlaybackService.play()
            }
            imageButtonPlayPause.setImageResource(resId)
        }
        imageButtonPlayPause.isEnabled = true
        imageButtonStop.setOnClickListener {
            mediaPlaybackService.stop()
            imageButtonPlayPause.setImageResource(R.drawable.ic_play_circle_outline_black_48dp)
            imageButtonPlayPause.isEnabled = false
            clearInfo()
        }
        seekBar.isEnabled = false
        seekBar.progress = 0
        seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mediaPlaybackService.seekTo(seekBar.progress)
            }
        })
        fab.setOnClickListener {
            val intent = Intent()
            intent.type = "audio/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Choose Track"), 1)
        }
    }

    override fun onResume() {
        applicationContext.bindService(
            Intent(
                applicationContext,
                MediaPlaybackService::class.java
            ), connection, Context.BIND_AUTO_CREATE
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            receiverElapsedTime,
            IntentFilter(MediaPlaybackService.MPS_RESULT)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            receiverCompleted,
            IntentFilter(MediaPlaybackService.MPS_COMPLETED)
        )
        super.onResume()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverElapsedTime)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverCompleted)
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val selectedTrack: Uri = data!!.data
            mediaPlaybackService.init(selectedTrack)
            initInfo(selectedTrack)
        }
    }

    private fun secondsToString(t: Int): String {
        val time: Int = t / 1000
        return String.format("%2d:%02d", time / 60, time % 60)
    }

    private fun initInfo(uri: Uri?) {
        if (uri != null) {
            val mData = MediaMetadataRetriever()
            mData.setDataSource(this, uri)
            val duration =
                mData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toInt()
            textViewTitle.text = mData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
            textViewArtist.text =
                mData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
            textViewDuration.text = secondsToString(duration)
            seekBar.max = duration
            seekBar.isEnabled = true
            try {
                val art = mData.embeddedPicture
                val image = BitmapFactory.decodeByteArray(art, 0, art.size)
                albumArt.setImageBitmap(image)
                val palette = Palette.from(image).generate()
                setColors(palette)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun updateElapsedTime(elapsedTime: Int) {
        seekBar.progress = elapsedTime
        textViewElapsedTime.text = secondsToString(elapsedTime)

        if (mediaPlaybackService.isPlaying()) {
            imageButtonPlayPause.isEnabled = true
            imageButtonPlayPause.setImageResource(R.drawable.ic_pause_circle_outline_black_48dp)
        }
    }

    private fun clearInfo() {
        textViewDuration.text = ""
        textViewElapsedTime.text = ""
        textViewTitle.text = "-"
        textViewArtist.text = "-"
        elapsedTime = 0
        seekBar.isEnabled = false
        seekBar.progress = 0
        albumArt.setImageResource(R.drawable.ic_album_white_400_128dp)
        imageButtonPlayPause.isEnabled = false
        imageButtonPlayPause.setImageResource(R.drawable.ic_play_circle_outline_black_48dp)
        setColors(null)
    }

    private fun setColors(palette: Palette?) {
        var colorPrimaryDark = ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark)
        var colorPrimary = ContextCompat.getColor(applicationContext, R.color.colorPrimary)
        var colorAccent = ContextCompat.getColor(applicationContext, R.color.colorAccent)

        if (palette != null) {
            setSupportActionBar(toolbar)
            colorPrimaryDark = palette.getDarkVibrantColor(
                ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark)
            )

            colorPrimary = palette.getVibrantColor(
                ContextCompat.getColor(applicationContext, R.color.colorPrimary)
            )

            colorAccent = palette.getLightVibrantColor(
                ContextCompat.getColor(applicationContext, R.color.colorAccent)
            )
        }

        val window: Window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = colorPrimaryDark
        toolbar.setBackgroundColor(colorPrimary)
        imageButtonPlayPause.setColorFilter(colorAccent)
        imageButtonStop.setColorFilter(colorAccent)
        seekBar.progressDrawable.setColorFilter(colorAccent, PorterDuff.Mode.SRC_IN)
        seekBar.thumb.setColorFilter(colorAccent, PorterDuff.Mode.SRC_IN)
        fab.backgroundTintList = ColorStateList.valueOf(colorAccent)
    }

    private val receiverElapsedTime = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            elapsedTime = intent.getIntExtra(MediaPlaybackService.MPS_MESSAGE, 0)
            updateElapsedTime(elapsedTime)
        }
    }

    private val receiverCompleted = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            clearInfo()
        }
    }

    private var connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mediaPlaybackService = (service as MediaPlaybackService.IDBinder).getService()
            isBinded = true
            initInfo(mediaPlaybackService.file)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isBinded = false
        }
    }
}
