package uz.xsoft.mplayerpaletteapi

import android.app.Service
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import java.io.IOException


class MediaPlaybackService : Service(), MediaPlayer.OnPreparedListener,
    MediaPlayer.OnCompletionListener {

    companion object {
        val MPS_MESSAGE = "uz.xsoft.mplayerpaletteapi.MediaPlaybackService.MESSAGE"
        val MPS_RESULT = "uz.xsoft.mplayerpaletteapi.MediaPlaybackService.RESULT"
        val MPS_COMPLETED = "uz.xsoft.mplayerpaletteapi.MediaPlaybackService.COMPLETED"
    }

    var mMediaPlayer: MediaPlayer? = null
    var file: Uri? = null

    inner class IDBinder : Binder() {
        fun getService(): MediaPlaybackService {
            return this@MediaPlaybackService
        }
    }

    var idBinder = IDBinder()

    var broadcastManager: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcastManager = LocalBroadcastManager.getInstance(this)
        super.onCreate()
    }

    override fun onDestroy() {
        stop()
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return idBinder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        stop()
        return super.onUnbind(intent)
    }

    fun init(file: Uri?) {
        this.file = file
        stop()
        try {
            mMediaPlayer = MediaPlayer()
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mMediaPlayer!!.setDataSource(applicationContext, file)
            mMediaPlayer!!.setOnPreparedListener(this)
            mMediaPlayer!!.setOnCompletionListener(this)
            mMediaPlayer!!.prepareAsync()
        } catch (e: IOException) {
            e.printStackTrace()
            stop()
        }
    }

    override fun onPrepared(mp: MediaPlayer) {
        mp.start()
        val updateThread = Thread(sendUpdates)
        updateThread.start()
    }

    private val sendUpdates = Runnable {
        while (mMediaPlayer != null) {
            sendElapsedTime()
            try {
                Thread.sleep(500)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    fun pause() {
        if (mMediaPlayer != null) mMediaPlayer!!.pause()
    }

    fun play() {
        if (mMediaPlayer != null) mMediaPlayer!!.start()
    }

    fun stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer!!.release()
            mMediaPlayer = null
            file = null
        }
    }

    fun seekTo(msec: Int) {
        mMediaPlayer!!.seekTo(msec)
    }

    fun isPlaying(): Boolean {
        return mMediaPlayer != null && mMediaPlayer!!.isPlaying
    }

    override fun onCompletion(mp: MediaPlayer?) {
        val intent = Intent(MPS_COMPLETED)
        broadcastManager!!.sendBroadcast(intent)
    }

    private fun sendElapsedTime() {
        val intent = Intent(MPS_RESULT)
        if (mMediaPlayer != null) intent.putExtra(MPS_MESSAGE, mMediaPlayer!!.currentPosition)
        broadcastManager!!.sendBroadcast(intent)
    }
}